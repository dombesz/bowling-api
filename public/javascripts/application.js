function refreshScore(){
  $.get('/current', function(data) {
      $('#current-game').html(data);
      setTimeout(refreshScore, 2000);
  });
}

$(document).ready(function() {
  refreshScore();
});