class BowlingApiController < Sinatra::Base
  use Rack::Flash
  set :public_folder, 'public'
  set :views, 'app/views'
  set :show_exceptions, :after_handler
  enable :sessions

  helpers do
    def json(data)
      JSON.dump(data)
    end
  end

  get '/' do
    @current_games = [Game.current].compact
    @past_games = Game.all(:finished?, :desc)
    slim :index
  end

  get '/current' do
    @current_games = [Game.current].compact
    slim :games, layout: false, locals: { games: @current_games }
  end

  get '/past' do
    @games = Game.all(:finished?, :desc)
    slim :games, layout: false, locals: { games: @games }
  end

  post '/create' do
    if Game.current
      flash[:warning] = 'The game is already on!'
    else
      GameService.start_new_game
      flash[:notice] = 'New game started!'
    end
    redirect to('/')
  end

  post '/score' do
    pass unless request.accept? 'application/json'
    content_type :json
    request.body.rewind
    payload = JSON.parse(request.body.read)
    GameService.register_score(payload["score"])
    json({ success: true })
  end

  error do
    if request.xhr?
      json({message: env['sinatra.error'].message})
    else
      'Sorry there was a nasty error - ' + env['sinatra.error'].message
    end
  end
end