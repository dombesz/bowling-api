class GamePresenter
  def initialize(game)
    @game = game
  end

  def name
    "Game ##{@game.id} "
  end

  def status
    "Status: " + (@game.finished? ? "Finished 🏁" : "Ongoing ⏳")
  end

  def total_score
    "Total score: #{@game.frames.sum(&:total_score)}"
  end

  def frames_table
    headers = ""
    scores = ""
    final_scores = ""
    10.times do |index|
      frame = @game.frames[index] || NullFrame.new
      headers << "<th colspan=\"6\">Frame #{index + 1}</th>"
      scores << "<td colspan=\"3\"></td>"
      scores << "<td colspan=\"3\">#{score_string_for(frame)}</td>"
      final_scores << "<td colspan=\"6\">#{frame.total_score}</td>"
    end

    "<tr>#{headers}</tr>" \
    "<tr>#{scores}</tr>" \
    "<tr>#{final_scores}</tr>"
  end

  def last_updated_at
    Time.now.strftime("last updated at %r")
  end

  private
  def score_string_for(frame)
    score_arr = []
    if frame.strike?
      score_arr << 'X'
      score_arr << frame.scores.last(2) if frame.extra_round_allowed?
    elsif frame.spare?
      score_arr << '/'
      score_arr << frame.scores.last if frame.extra_round_allowed?
    else
      score_arr << frame.scores
    end
    score_arr.join(',')
  end
end