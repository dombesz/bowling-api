require 'yaml/store'
require 'singleton'
require 'forwardable'

class GameStore
  include Singleton
  attr_reader :store
  class EntryNotFoundError < StandardError; end

  STORE_NAME = ENV['STORE_NAME']
  STORE_DEFAULT_VALUES = { entries: {}, last_id: 0 }

  class << self
    extend Forwardable
    # Delegate all the class method calls to the singleton instance
    def_delegators :instance, :find, :save, :all, :current
  end

  # Initialize the store reader
  # @return [void]
  def initialize
    @store = load_store
  end

  # Returns the game object based on an id
  # Raises an EntryNotFound exception if the game object cannot be found
  # @param [Integer] game_id
  # @return [Game]
  def find(game_id)
    find_all[game_id] || raise(EntryNotFoundError, "Game with id #{game_id} does not exist.")
  end

  # Returns all the games stored in the YAML::Store,
  # `filter` can be a method name on the object
  # `sort` will reverse the order of the objects
  # @param [Symbol] filter
  # @param [Symbol] sort (:asc/:desc)
  # @return [Array<Game>]
  def all(filter=nil, sort=:asc)
    values = find_all.values
    values.select!(&filter) if filter
    values.reverse! if sort == :desc
    values
  end


  # Finds the first unfinished game and returns
  # @return [Game]
  def current
    all.find { |g| !g.finished? }
  end

  # Stores a game object and attaches an id to it
  # Raises an ArgumentError if the game object is not a [Game]
  # @param [Game]
  # @return [Game]
  def save(game)
    raise ArgumentError unless game.is_a?(Game)
    store.transaction do
      game.id ||= store[Game][:last_id] += 1
      store[Game][:entries][game.id] = game
    end
  end

  private

  def load_store
    YAML::Store.new(STORE_NAME).tap do |store|
      store.transaction { store[Game] ||= STORE_DEFAULT_VALUES }
    end
  end

  def find_all
    store.transaction do
      store[Game][:entries]
    end
  end
end