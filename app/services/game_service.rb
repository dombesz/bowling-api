module GameService
  class GameNotStartedError < StandardError; end

  # Start a new game, it returns the game object stored in the database
  # @return [Game]
  def self.start_new_game
    return GameStore.current if GameStore.current
    game = Game.new
    game.save
    game
  end

  # Register score takes care of adding the next score to a Game
  # It opens new frames automatically and returns GameAlreadyFinishedError when the game is over
  # @param [Game] game
  # @param [Integer] score
  # @return [void]
  def self.register_score(score)
    game = GameStore.current
    raise GameNotStartedError, "No Game is running" unless game
    game.add_score(score)
    game.save
  end
end
