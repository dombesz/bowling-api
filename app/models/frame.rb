class Frame
  attr_accessor :scores
  attr_accessor :bonus
  attr_accessor :last_frame

  class FrameAlreadyFinishedError < StandardError; end

  ROUND_ONE = 1
  ROUND_TWO = 2
  ROUND_THREE = 3
  MAX_PINS = 10

  STRIKE_FRAME = :strike_frame
  SPARE_FRAME = :spare_frame
  NORMAL_FRAME = :normal_frame

  EXTRA_FRAME_ALLOWED_STATUSES = [STRIKE_FRAME, SPARE_FRAME]

  def initialize(scores: [], last_frame: false)
    @scores = scores
    @last_frame = last_frame
  end

  def add_score(score)
    validate_frame!
    validate_score!(score)
    scores << score
  end

  def finished?
    extra_round_allowed? ? extra_round_finished? : normal_rounds_finished?
  end

  def first_round_finished?
    round_number == ROUND_ONE
  end

  def normal_rounds_finished?
    (strike? && !extra_round_allowed?) || second_round_finished?
  end

  def second_round_finished?
    round_number == ROUND_TWO
  end

  def extra_round_finished?
    round_number == ROUND_THREE
  end

  def score
    scores.sum
  end

  def total_score
    score + bonus.to_i
  end

  def strike?
    status == STRIKE_FRAME
  end

  def spare?
    status == SPARE_FRAME
  end

  def extra_round_allowed?
    last_frame && EXTRA_FRAME_ALLOWED_STATUSES.include?(status)
  end

  private

  def round_number
    scores.size
  end

  def status
    return STRIKE_FRAME if scores.take(ROUND_ONE).sum == MAX_PINS
    return SPARE_FRAME  if scores.take(ROUND_TWO).sum == MAX_PINS
    return NORMAL_FRAME
  end

  def validate_frame!
    raise FrameAlreadyFinishedError if finished?
  end

  def validate_score!(new_score)
    max_allowed = if strike? && last_frame
      MAX_PINS * 3
    elsif spare? && last_frame
      MAX_PINS * 2
    else
      MAX_PINS
    end

    unless new_score.is_a?(Integer) && (new_score + score).between?(0, max_allowed)
      raise ArgumentError, "Total score must be between 0 and #{max_allowed}"
    end
  end
end