require 'forwardable'

class Game
  attr_accessor :id
  attr_accessor :frames

  class GameAlreadyFinishedError < StandardError; end

  MAX_FRAME_SIZE = 10

  class << self
    extend Forwardable
    # Delegate all the class method calls to the singleton instance
    def_delegators 'GameStore', :find, :all, :current, :finished
  end

  def initialize(args={})
    @frames = args.fetch(:frames, [Frame.new])
  end

  def add_score(score)
    update_frames
    current_frame.add_score(score)
    update_frame_bonuses
  end

  def save
    validate!
    GameStore.save(self)
  end

  def finished?
    frames.size >= MAX_FRAME_SIZE
  end

  private

  def current_frame
    frames.last
  end

  def previous_frame
    frames[-1]
  end

  def second_previous_frame
    frames[-2]
  end

  def update_frames
    current_frame.finished? ? advance_frame : current_frame
  end

  def advance_frame
    raise GameAlreadyFinishedError if frames.size == MAX_FRAME_SIZE
    frames << Frame.new(last_frame: (frames.size + 1) == MAX_FRAME_SIZE)
  end

  def validate!
    raise GameAlreadyFinishedError if frames.size > MAX_FRAME_SIZE
  end

  def update_frame_bonuses
    previous_frame = frames[-2]
    return unless previous_frame
    second_previous_frame = frames[-3]

    if previous_frame.strike?
      previous_frame.bonus = current_frame.score
      if second_previous_frame&.strike? && current_frame.first_round_finished?
        second_previous_frame.bonus += current_frame.score
      end
    elsif previous_frame.spare? && current_frame.first_round_finished?
      previous_frame.bonus = current_frame.score
    end
  end
end