require "spec_helper"
RSpec.describe Game do

  describe "#add_score" do
    let(:game) { Game.new }

    it 'is expected to add 5' do
      game.add_score(5)
      expect(game.frames.size).to eq(1)
      expect(game.frames.first.score).to eq(5)
    end

    it 'is expected to add 10' do
      game.add_score(10)
      expect(game.frames.size).to eq(1)
      expect(game.frames.first.score).to eq(10)
    end

    it 'is expected to add all scores' do
      10.times { game.add_score(10) }
      expect(game.frames.size).to eq(10)
      expect(game.frames.sum(&:score)).to eq(100)
      expect(game.frames.sum(&:total_score)).to eq(270)
    end

    it 'is expected to add score of perfect game' do
      12.times { game.add_score(10) }
      expect(game.frames.size).to eq(10)
      expect(game.frames.sum(&:score)).to eq(120)
      expect(game.frames.sum(&:total_score)).to eq(310)
    end

    it 'is expected to add score of perfect frame with spare ending' do
      11.times { game.add_score(10) }
      game.add_score(5)
      expect(game.frames.size).to eq(10)
      expect(game.frames.sum(&:score)).to eq(115)
      expect(game.frames.sum(&:total_score)).to eq(300)
    end

    it 'is expected to add score of perfect frame with spare ending frame' do
      10.times { game.add_score(10) }
      2.times { game.add_score(5) }
      expect(game.frames.size).to eq(10)
      expect(game.frames.sum(&:score)).to eq(110)
      expect(game.frames.sum(&:total_score)).to eq(290)
    end

    it 'is expected to add score of all spare frames' do
      21.times { game.add_score(5) }
      expect(game.frames.size).to eq(10)
      expect(game.frames.sum(&:score)).to eq(105)
      expect(game.frames.sum(&:total_score)).to eq(150)
    end

    context 'errors' do
      context 'GameAlreadyFinishedError' do
        it 'is expected to error on perfect game plus 1' do
          12.times { game.add_score(10) }
          expect{
            game.add_score(10)
          }.to raise_error(Game::GameAlreadyFinishedError)
        end

        it 'is expected to error on perfect frame with spare ending plus 1' do
          11.times { game.add_score(10) }
          game.add_score(5)
          expect{
            game.add_score(10)
          }.to raise_error(Game::GameAlreadyFinishedError)
        end

        it 'is expected to error on perfect frame with spare ending frame plus 1' do
          10.times { game.add_score(10) }
          2.times { game.add_score(5) }
          expect{
            game.add_score(10)
          }.to raise_error(Game::GameAlreadyFinishedError)
        end

        it 'is expected to error on all spare frames plus 1' do
          21.times { game.add_score(5) }
          expect{
            game.add_score(10)
          }.to raise_error(Game::GameAlreadyFinishedError)
        end
      end

      context 'ArgumentError for score' do
        it 'is expected error on 11' do
          expect{
            game.add_score(11)
          }.to raise_error(ArgumentError, 'Total score must be between 0 and 10')
        end

        it 'is expected to error on -1' do
          expect{
            game.add_score(-1)
          }.to raise_error(ArgumentError, 'Total score must be between 0 and 10')
        end

        it 'is expected to error on 5+7' do
          game.add_score(5)
          expect{
            game.add_score(7)
          }.to raise_error(ArgumentError, 'Total score must be between 0 and 10')
        end

        context 'on last frame' do
          it 'is expected to error on perfect game ending with 11' do
            11.times { game.add_score(10) }
            expect{
              game.add_score(11)
            }.to raise_error(ArgumentError, 'Total score must be between 0 and 30')
          end

          it 'is expected to error on perfect game ending with 11' do
            10.times { game.add_score(10) }
            game.add_score(6)
            expect{
              game.add_score(6)
            }.to raise_error(ArgumentError, 'Total score must be between 0 and 30')
          end
        end
      end
    end
  end
end
