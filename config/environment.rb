require 'rubygems'
require 'bundler/setup'

Bundler.require(:default)                   # load all the default gems
Bundler.require(Sinatra::Base.environment)  # load all the environment specific gems

ENV['STORE_NAME'] = if ENV['RACK_ENV'] == 'test'
                      'spec/game.store.test'
                    else
                      'db/game.store'
                    end

require 'rack-flash'
require_all 'app'
