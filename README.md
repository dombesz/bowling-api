# Welcome to the Bowling

## Setup
  - `bundle install`
  - `rackup`

## Usage
### Start the Game
  - `visit http://localhost:9292`
  - `click 'Start new game'`

  The server will start a new game and starts polling for changes.

### Add scores from the api
  - `curl -X POST -i http://localhost:9292/score --data '{"score": 5}'`

  Choose the score of your liking between 0..10

### Run the specs
  - `rspec`

## Notes

I just tested only the essential score calculation api, and the classes are not tested in isolation in order to have a more thorough testing touching all the additional classes. It might not be the best solution for big applications and I would make this more tidy if I had the time. But for the sake of demonstration it is sufficient.

Sinatra is not a framework that I often use, I chose it because I wanted something lightweight but midway I kind of regretted it because I spent some time cooking up the configuration for it.
Another thing that I overshoot was the GameStore object, I didn't wanted to use a database and I thought it would be cool to implement a mini store using ruby's YAML::Store implementation of PStore. It turned out to be a huge time consumer too.🤷‍♂️

The table styling is a shameless plug from [here](https://codepen.io/owenjam/pen/reeLWN)